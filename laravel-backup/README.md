# Laravel Automated backup to AWS S3-bucket

## Introduction
This document represents a guide to setup an automated backup system for customers of Provide Tailored Software which are using the Laravel application stack.
This document explains the use of:

* `spatie/laravel-backup` [https://github.com/spatie/laravel-backup](https://github.com/spatie/laravel-backup)
* `spatie/nova-backup-tool` [https://github.com/spatie/nova-backup-tool](https://github.com/spatie/nova-backup-tool) (if using Laravel Nova)


## Prerequisites
In order to complete this guide, it assumes you have the following:

* A Laravel or Laravel Nova project to backup.
* Access to a AWS S3-bucket.
* The tool `mysqldump` installed.

## Installation
Instructions taken from: [https://spatie.be/docs/laravel-backup/v6/installation-and-setup](https://spatie.be/docs/laravel-backup/v6/installation-and-setup)

Install using composer:
> `composer require spatie/laravel-backup`

Then publish the config file to `config/backup.php`
> `php artisan vendor:publish --provider="Spatie\Backup\BackupServiceProvider"`

By default the `config/backup.php` is configured to backup all files and the `mysql` database to `storage/public/laravel_backup`

We can now use the commands in `php artisan backup:`: `run`, `clean`, `list`, `monitor`

## Configuration
### Backup to S3 bucket
#### Step 1: Create AWS Bucket and User
First login to AWS Managment Console and create an AWS S3-Bucket that suits your needs.
We use `laravel-backup-provide-test` as an example.

Then go the IAM (Identity and Access Managment) to create a new user. Make sure the user has only `programmatic access`

![](images/aws-create-user.png)

In the persmissions screen create a new policy with these values and replace `laravel-backup-provide-test` with the correct name of your bucket. This policy gives limited read and write access to the user on that bucket. 

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket",
                "s3:GetBucketLocation"
            ],
            "Resource": "arn:aws:s3:::laravel-backup-provide-test"
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:DeleteObject"
            ],
            "Resource": "arn:aws:s3:::laravel-backup-provide-test/*"
        }
    ]
}
```

Skip the next screens and create the user.
Write down the given `Access key ID` and the `Secret access key`

#### Step 2: Use Laravel S3 disk
Open the `.env` file in your Laravel project and update these values:

```
AWS_ACCESS_KEY_ID=<Your Access Key Id>
AWS_SECRET_ACCESS_KEY=<Your Secret Access Key>
AWS_DEFAULT_REGION=<Bucket region name>
AWS_BUCKET=<Your bucket name>
```

To test if the S3 connection is setup correctly open up tinker:

```
php artisan tinker
Storage::disk('s3')->files();
```

This should return any files or an empty array:

```
>>> Storage::disk('s3')->files();
=> []
```

#### Step 4: Configure backup to S3
Open `config/backup.php` and scroll to the `destination`-array entry and change/add `disks` to contain `s3` as a destination.

The backups will now be stored in S3-Bucket! :)


### Schedule

```php
// app/Console/Kernel.php

protected function schedule(Schedule $schedule)
{
   $schedule->command('backup:clean')->daily()->at('01:00');
   $schedule->command('backup:run')->daily()->at('02:00');
}
```





